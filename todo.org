* In general
* TODO Mode selector

  Complications; toggle boxes are typically ON/OFF only, this requires a custom checkbox that
  shows the ON state on either resturant or grocery shopping

* Resturants
  https://data.gov.sg/dataset/healthier-eateries
** DONE Convert data to suitable format for listing
   CLOSED: [2018-11-23 Fri 17:42]
   :LOGBOOK:
   - State "DONE"       from "TODO"       [2018-11-23 Fri 17:42]
   :END:
** DONE Resturants listing display
   CLOSED: [2018-11-23 Fri 17:42]
   :LOGBOOK:
   - State "DONE"       from "TODO"       [2018-11-23 Fri 17:42]
   :END:
** TODO Process data to retrieve an image from YELP / Google? Find a filler image if one doesn't exist
** TODO Click on resturant popup
*** TODO Resturant name
*** TODO Design how details should be shown
**** TODO Pull data from YELP -> display on UI
*** TODO Redirection to gmaps to location
    We are delegating the mapping stuff to gmaps here
** TODO Filter
*** DONE Design of the shitty ass filter
    CLOSED: [2018-11-25 Sun 22:27]
    :LOGBOOK:
    - State "DONE"       from ""           [2018-11-25 Sun 22:27]
    :END:
*** Types of filter
    For each filter, create a select dropdown that shows available filters for that category

    HALAL
    CUISINE
    VEGAN
    STARS (Reviews)
    Distance
** HOLD Filter selection panel
   Do we want to keep this? I think the panel on the left already does a pretty decent job TBH
** DONE Rating filter
   CLOSED: [2018-11-26 Mon 16:16]
   :LOGBOOK:
   - State "DONE"       from "TODO"       [2018-11-26 Mon 16:16]
   :END:
** DONE Price filter
   CLOSED: [2018-11-26 Mon 16:16]
   :LOGBOOK:
   - State "DONE"       from "TODO"       [2018-11-26 Mon 16:16]
   :END:
* Grocery shopping

  Can try to lookup nutrition info from product name
  Generate recipe from the ingredients that you have selected

  https://data.gov.sg/dataset/hcs-product-list

  SUpermarket KML; cross reference with HCS supermarket names
** DONE Food group filter
   CLOSED: [2018-11-26 Mon 17:37]
   :LOGBOOK:
   - State "DONE"       from "TODO"       [2018-11-26 Mon 17:37]
   :END:
** DONE Company name filter
   CLOSED: [2018-11-26 Mon 17:37]
   :LOGBOOK:
   - State "DONE"       from "TODO"       [2018-11-26 Mon 17:37]
   :END:
** TODO Cross reference supermarket KML

* Demo flow
** Resturants
*** TODO Images from YELP
