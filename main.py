from flask import Flask, render_template, send_from_directory, request
import feather
import haversine

app = Flask(__name__)

eateries = feather.read_dataframe('data/eateries.feather')
groceries = feather.read_dataframe('data/hcs.feather')
supermarkets = feather.read_dataframe('data/supermarkets.feather')

@app.route("/static/<path:path>")
def staticFiles(path):
    return send_from_directory('static', path)

@app.route("/")
def index():
    return render_template("index.html")

def filter_dataframe(pos, distance, attributes, cuisine, rating, price):
    c = eateries.copy()
    c['distance'] = c.apply(lambda x: haversine.distance((float(pos[0]), float(pos[1])), (x['lat'], x['lon'])), axis=1)

    distance_mask = c['distance'] <= distance
    final = c[distance_mask]

    if len(attributes) > 0:
        attr_mask = final['ATTRIBUTE'].isin(attributes)
        final = final[attr_mask]

    if len(cuisine) > 0:
        rating_mask = final['RATING'] >= rating
        final = final[rating_mask]

    price_mask = final['PRICE'] >= price
    final = final[price_mask]

    return final

@app.route("/resturants/lat/<lat>/lng/<lng>")
def resturants(lat, lng):
    distance = float(request.args.get('distance', "1000")) / 1000
    filter_attributes = request.args.getlist("attributes")
    filter_cuisines = request.args.getlist("cuisines")
    rating = int(request.args.get('rating', "0"))
    price = int(request.args.get('price', "0"))

    zz = filter_dataframe((lat,lng), distance, filter_attributes, filter_cuisines, rating, price).to_dict('records')

    selections = filter_attributes + filter_cuisines
    return render_template("/resturants.html",
                           resturants=zz,
                           selections=selections,
                           attributes=["Halal", "Vegan friendly"],
                           filter_attributes=filter_attributes,
                           cuisines=["Cuisine 1", "Cuisine 2"],
                           filter_cuisines=filter_cuisines,
                           lat=lat, lon=lng, price=price, rating=rating)

@app.route("/resturants/iframe/lat/<lat>/lng/<lng>")
def resturantsIFrame(lat, lng):
    distance = float(request.args.get('distance', "1000")) / 1000
    filter_attributes = request.args.getlist("attributes")
    filter_cuisines = request.args.getlist("cuisines")
    rating = int(request.args.get('rating', '0'))
    price = int(request.args.get('price', '0'))

    zz = filter_dataframe((lat,lng), distance, filter_attributes, filter_cuisines, rating, price).to_dict('records')

    return render_template("/resturants_iframe.html",
                           resturants=zz)

def get_supermarkets_in_range(supermarkets, pos, distance):
    supermarket_copy = supermarkets.copy()
    supermarket_copy['distance'] = supermarket_copy.apply(lambda x: haversine.distance((float(pos[0]),
                                                                                   float(pos[1])),
                                                                                  (x['lat'], x['lon'])), axis=1)
    supermarket_copy = supermarket_copy[supermarket_copy['distance'] <= distance]

    return supermarket_copy

def filter_groceries(pos, supermarkets, filter_food_groups, filter_brand_names, filter_supermarkets):
    c = groceries.copy()
    c = c[c['license_no'].isin(supermarkets['LIC_NO'])]

    if len(filter_food_groups) > 0:
        food_groups_mask = c['main_food_group'].isin(filter_food_groups)
        c = c[food_groups_mask]

    if len(filter_brand_names) > 0:
        brand_names_mask = c['brand_name'].isin(filter_brand_names)
        c = c[brand_names_mask]

    if len(filter_supermarkets) > 0:
        name_to_license = supermarkets[supermarkets['LIC_NAME'].isin(filter_supermarkets)]['LIC_NO']
        supermarket_license_mask = c['license_no'].isin(name_to_license)
        c = c[supermarket_license_mask]

    return c

@app.route("/grocery/lat/<lat>/lng/<lng>")
def grocery(lat, lng):
    distance = float(request.args.get('distance', "1000")) / 1000

    supermarket_copy = get_supermarkets_in_range(supermarkets, (lat, lng), distance)
    supermarket_names = set(supermarket_copy.groupby('LIC_NAME').groups.keys())

    filter_food_groups = request.args.getlist("food_groups")
    filter_brand_names = request.args.getlist('brand_names')
    filter_supermarkets = request.args.getlist('supermarkets')

    c = filter_groceries((lat, lng), supermarket_copy, filter_food_groups, filter_brand_names, filter_supermarkets)
    food_groups = set(c.groupby('main_food_group').groups.keys())
    brand_names = set(c.groupby('brand_name').groups.keys())
    return render_template("/grocery.html",
                           groceries=c.to_dict('records'),
                           brand_names=brand_names,
                           food_groups=food_groups,
                           supermarket_names=supermarket_names,
                           distance=distance,
                           active_foodgroups=filter_food_groups,
                           active_brands=filter_brand_names,
                           active_supermarkets=filter_supermarkets,
                           lat=lat, lon=lng)

@app.route("/grocery/iframe/lat/<lat>/lng/<lng>")
def groceryIFrame(lat, lng):
    distance = float(request.args.get('distance', "1000")) / 1000
    food_groups = list(groceries.groupby('main_food_group').groups.keys())
    brand_names = list(groceries.groupby('brand_name').groups.keys())

    supermarket_copy = get_supermarkets_in_range(supermarkets, (lat, lng), distance)
    supermarket_names = set(supermarket_copy.groupby('LIC_NAME').groups.keys())

    filter_food_groups = request.args.getlist("food_groups")
    filter_brand_names = request.args.getlist('brand_names')
    filter_supermarkets = request.args.getlist('supermarkets')

    c = filter_groceries((lat, lng), supermarket_copy, filter_food_groups, filter_brand_names, filter_supermarkets)

    return render_template("/groceries_iframe.html",
                           groceries=c.to_dict('records'))
if __name__ == '__main__':
    app.run()
