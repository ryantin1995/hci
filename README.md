# Healthier Eating Lifestyle
This web application is for our Human Computer Interaction Project

# Getting Started
These instructions will get you running the server.

## Prerequisites
Our project requires these following software, please install it before proceeding.

- Google Chrome
- Python 3.6

## Packages Needed
To purely run the application on your web browser we will require the python framework Flask. To install flask run the following command
```
pip install flask pandas feather-format
```

# Running the website
To run the website, clone this repo and cd into your local directory.
Before we begin, we need to set some environment variables

```
python main.py
```

Go into google chrome and access the site with the address
```
http://localhost:5000
```

# Additional
## Running the pre-processing
To run the pre-processing data please install the following packages by running the command
```
pip install tika pandas feather-format fastkml numpy jupyter
```
cd into the repo directory and run
```
jupyter notebook
```
A website should pop-up. Please select the `generate_fakedata.ipynb` to access our pre-processing
