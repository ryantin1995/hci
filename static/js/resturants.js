var distance;
var shownResturant = {};

(function() {
  registerListeners();

  distance = document.getElementById("distance-slider").value;
  document.getElementById("distance-slider").addEventListener("input", () => {
    let v = document.getElementById("distance-slider").value;
    let prettyValue = v / 1000;

    document.getElementById("distance-label").textContent = `${prettyValue} km`;
  });

  $(".filter-rating-star").on("starrr:change", (e, value) => {
    rating = value || 0;
    reloadListing();
  });
  $(".filter-price-star").on("starrr:change", (e, value) => {
    price = value || 0;
    reloadListing();
  });
})();

function buildNewURL() {
  let params = new URLSearchParams();
  active_attributes.forEach((attr) => params.append("attributes", attr));
  active_cuisines.forEach((attr) => params.append("cuisines", attr));
  params.append("distance", distance);
  params.append("rating", rating);
  params.append("price", price);

  // DIRTY HACK!!!!
  let pathname = location.pathname;
  pathname = pathname.replace("resturants", "resturants/iframe");

  let newURL = `${location.protocol}//${location.hostname}:${location.port}${pathname}?${params.toString()}`;
  return newURL;
}

function attributeChanged(attr) {
  let inList = active_attributes.indexOf(attr) >= 0;
  if (inList) {
    active_attributes = active_attributes.filter((x) => x !== attr);
  } else {
    active_attributes.push(attr);
  }
  reloadListing();
}

function cuisineChanged(cuisine) {
  let inList = active_cuisines.indexOf(cuisine) >= 0;
  if (inList) {
    active_cuisines = active_cuisines.filter((x) => x !== cuisine);
  } else {
    active_cuisines.push(cuisine);
  }
  reloadListing();
}

function resturantItemClicked(r) {
  shownResturant = r;
}

function reloadListing() {
  const url = buildNewURL();

  fetch(url)
    .then((d) => d.text())
    .then(res => {
      document.getElementById("resturants-display").innerHTML = res;
      rebindModal();
    });
}

function buildBody(r) {
  let distance_pretty = parseFloat(r.distance * 1000).toFixed(0);
  let rating_stars = [];
  for (let i=1; i < 5+1; i++) {
    const c = i <= r.RATING ? 'fa fa-star' : 'far fa-star';
    rating_stars.push(`<i class="${c}""></i>`);

  }
  let dollar_signs = [];
  for (let i=1; i < 5+1; i++) {
    const c = i <= r.PRICE ? "fas fa-dollar-sign" : "";
    dollar_signs.push(`<i class="${c}"></i>`);
  }
  return `<div class="container-fluid">
        <div class="row">${r.NAME}</div>
        <div class="row">Type: ${r.ATTRIBUTE !== null ? r.ATTRIBUTE : "Uncategorized"}</div>
        <div class="row">Distance: ${distance_pretty}m</div>
        <div class="row">Rating: ${rating_stars.join('\n')}</div>
        <div class="row">Price: ${dollar_signs.join('\n')}</div>
    </div>`;
}

function rebindModal() {
  $("#resturant-modal").on("show.bs.modal", e => {
    let r = shownResturant;
    let directionsURL = `https://www.google.com/maps/dir/${pos[0]},${pos[1]}/${r.lat},${r.lon}`;

    document.getElementById("resturant-modal-title").textContent = r.NAME;
    document.getElementById("resturant-modal-body").innerHTML = buildBody(r);

    document.getElementById("resturant-modal--directions-button").href = directionsURL;
  });
}

function registerListeners() {
  document.getElementById("distance-slider").addEventListener("change", () => {
    let value = document.getElementById("distance-slider").value;
    distance = value;

    reloadListing();
  }, false);
  rebindModal();
}
