var shownGrocery = {};

(function() {
  $(".selectpicker").selectpicker();

  $("#food-groups--selector").on('change', () => {
    active_foodgroups = $("#food-groups--selector").val();
    reloadListing();
  });
  $("#brand-names--selector").on('change', () => {
    active_brands = $("#brand-names--selector").val();
    reloadListing();
  });
  $("#supermarkets--selector").on('change', () => {
    active_supermarkets = $("#supermarkets--selector").val();
    reloadListing();
  });

  document.getElementById("distance-slider").addEventListener("change", () => {
    let value = document.getElementById("distance-slider").value;
    distance = value;

    reloadPage();
  }, false);

  document.getElementById("distance-slider").addEventListener("input", () => {
    let v = document.getElementById("distance-slider").value;
    let prettyValue = v / 1000;

    document.getElementById("distance-label").textContent = `${prettyValue} km`;
  });

  rebindModal();
})();

function buildNewIFrameURL() {
  let params = new URLSearchParams();
  active_foodgroups.forEach((attr) => params.append("food_groups", attr));
  active_brands.forEach((attr) => params.append("brand_names", attr));
  active_supermarkets.forEach((attr) => params.append("supermarkets", attr));

  params.append("distance", distance);

  // DIRTY HACK!!!!
  let pathname = location.pathname;
  pathname = pathname.replace("grocery", "grocery/iframe");

  let newURL = `${location.protocol}//${location.hostname}:${location.port}${pathname}?${params.toString()}`;
  return newURL;
}

function buildNewURL(url) {
  return url.replace("grocery/iframe", "grocery");
}

function reloadPage() {
  const newURL = `${location.protocol}//${location.hostname}:${location.port}${location.pathname}?distance=${distance}`;

  window.location.href = newURL;
}

function reloadListing() {
  const url = buildNewIFrameURL();

  fetch(url)
    .then((d) => d.text())
    .then(res => {
      document.getElementById("groceries-display").innerHTML = res;
      rebindModal();
    });

}

function groceryItemClicked(g) {
  shownGrocery = g;
}

function valueIfNotFalsy(v, postfix, falsyValue) {
  if (v === undefined || v === null || isNaN(v)) {
    return falsyValue;
  }
  return v + postfix;
}
function buildBody(r) {
  let directionsURL = `https://www.google.com/maps/dir/${pos[0]},${pos[1]}/${r.license_lat},${r.license_lng}`;
  return `
<div class="container-fluid">
  <div class="row">
  <table class="table table-borderless">
    <tbody>
      <tr>
        <th class="table-field">Serving size</th>
        <td>${valueIfNotFalsy(r.serving_size, "", "No information")}</td>
      </tr>
      <tr>
        <th>Calories</th>
        <td>${valueIfNotFalsy(r.calories, " kcal", "No information")}</td>
      </tr>
      <tr>
        <th>Fats</th>
        <td>${valueIfNotFalsy(r.fats, " g", "No information")}</td>
      </tr>
      <tr>
        <th>Carbohydrates</th>
        <td>${valueIfNotFalsy(r.carbs, " g", "No information")}</td>
      </tr>
      <tr>
        <th>Protein</th>
        <td>${valueIfNotFalsy(r.protein, " g", "No information")}</td>
      </tr>
      <tr>
        <th>Found at</th>
        <td>
            <a href=${directionsURL}
               rel="noopener noreferer"
               title="Directions to ${r.license_name}"
               target="_blank">${r.license_name}</a>
        </td>
      </tr>
    </tbody>
  </table>
  </div>
</div>
`;
}

function rebindModal() {
  $("#grocery-modal").on("show.bs.modal", e => {
    let r = shownGrocery;

    document.getElementById("grocery-modal-title").textContent = r.product_name;
    document.getElementById("grocery-modal-body").innerHTML = buildBody(r);

    //document.getElementById("grocery-modal--directions-button").href = directionsURL;
  });
}
