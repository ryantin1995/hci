var globalLat
var globalLng

document.getElementById("booey").addEventListener("click", () => {
    if ("geolocation" in navigator) {
	    navigator.geolocation.getCurrentPosition(function(position) {
            globalLat = position.coords.latitude;
            globalLng = position.coords.longitude;
            setReverseGeocodingData(globalLat, globalLng);
        });	
    }
})

document.addEventListener('DOMContentLoaded', function() {
    if ("geolocation" in navigator) {
	    navigator.geolocation.getCurrentPosition(function(position) {
            globalLat = position.coords.latitude;
            globalLng = position.coords.longitude;
            setReverseGeocodingData(globalLat, globalLng);
        });	
    }
}, false);

function setReverseGeocodingData(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    // This is making the Geocode request
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status !== google.maps.GeocoderStatus.OK) {
            alert(status);
        }
        // This is checking to see if the Geoeode Status is OK before proceeding
        if (status == google.maps.GeocoderStatus.OK) {
			var address = (results[0].formatted_address);
			document.getElementById("location--input").value = address;
        }
    });
}

var input = document.getElementById('location--input');
var options = {
  componentRestrictions: {country: 'sg'}
};

autocomplete = new google.maps.places.Autocomplete(input, options);

document.getElementById("restaurant-button").addEventListener("click", () =>{

    if(document.getElementById("location--input").value != ""){
        var place = autocomplete.getPlace();
        var lat = globalLat;
        var lng = globalLng;
        if(place != undefined){
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
        }
        window.location.href = `/resturants/lat/${lat}/lng/${lng}`;
    }
});
document.getElementById("grocery-button").addEventListener("click", () => {
    if(document.getElementById("location--input").value != ""){
        var place = autocomplete.getPlace();
        var lat = globalLat;
        var lng = globalLng;
        if(place != undefined){
            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();
        }
        window.location.href = `/grocery/lat/${lat}/lng/${lng}`;
    }
});








