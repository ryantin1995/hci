import fastkml as kml
import re
import collections
import pandas as pd

# https://github.com/lychee512/py_sg_map/blob/master/py_sg_map.py
def convertToDf(placemarks):
    df = pd.DataFrame()
    for i, placemark in enumerate(placemarks):
        try:
            data = placemark.extended_data.elements[0].data
            # build a dict from the name/value pairs in data
            # note that we encapsulated the values as a 1-element list
            data_dict = dict([(_dict['name'], [_dict['value']]) for _dict in data])

            # remove these fields from the dict
            data_dict.pop('FMEL_UPD_D', None)
            data_dict.pop('INC_CRC', None)

        except AttributeError:  # catch when extended_data is none
            data_dict = dict()

        # add a polygon field to the dict
        data_dict['lon'] = placemark.geometry.x
        data_dict['lat'] = placemark.geometry.y

        # build a dataframe and append it
        _df = pd.DataFrame(data_dict, index=[i])
        df = df.append(_df, sort=True)
    df = df.apply(pd.to_numeric, errors='ignore')

    return df

def main():
    with open('data/healthier-eateries-kml.kml', 'rt', encoding='utf-8') as f:
        doc = f.read()
    k = kml.KML()
    pattern = re.compile(r'xsd:')
    doc2 = re.sub(pattern, '', doc)
    k.from_string(doc2.encode('utf-8'))

    document = list(k.features())[0]
    folder = list(document.features())[0]
    placemarks = list(folder.features())

    parsed_df = convertToDf(placemarks)
    parsed_df.to_feather("data/eateries.feather")
if __name__ == '__main__':
    main()
